TEMPLATE = app
QT += gui core widgets network

CONFIG += c++11

INCLUDEPATH += ../../UNO

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
#isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=  $$PWD/../lib
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}

SOURCES +=     main.cpp     \
	test_card.cpp	\
	../../UNO/sources/card.cpp \
	../../UNO/sources/wildcard.cpp \
	../../UNO/sources/game.cpp \
	../../UNO/sources/deck.cpp \
	../../UNO/sources/pile.cpp \
	../../UNO/sources/player.cpp \
	../../UNO/sources/pc.cpp \
	../../UNO/sources/human.cpp \
	test_deck.cpp \
	test_pc.cpp \
	test_pile.cpp \
	test_player.cpp \
	test_game.cpp \
	test_wildcard.cpp \
	test_human.cpp

HEADERS += ../../UNO/headers/card.h \
	../../UNO/headers/wildcard.h \
	../../UNO/headers/game.h \
	../../UNO/headers/deck.h \
	../../UNO/headers/pile.h \
	../../UNO/headers/player.h \
	../../UNO/headers/pc.h \
	../../UNO/headers/human.h
