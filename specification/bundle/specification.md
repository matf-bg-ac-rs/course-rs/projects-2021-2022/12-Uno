# Project Uno

**UNO** is a multiplayer card game application that can be played against bots or against real people with the player amount being either 2, 4 or 8. The goal of the game is for the player to rid himself of all the cards in his hand and while doing so, earn points by placing cards on the pile which is how the final score of that player for that game is determined. More information on how to play the game in **How to play** section.

## How to setup

### Prerequisites

- gcc / g++
- Qt 6.2.2
- Qt6Multimedia

First of all, download package information from all configured sources by running
```bash
   $ sudo apt-get update
```

Next, install gcc / g++ by typing:
```bash
   $ sudo apt-get -y install gcc g++
```

Unfortunately, Qt6 and Qt Multimedia Add-On module can **only** (on Ubuntu) be added from the MaintananceTool (located in Qt's install directory) because you can't install it using Ubuntu's packaging tool (apt).

### Setup

**GUI**

Open the project in QtCreator, setup your kits so it's using Desktop Qt 6.2.2 GCC 64-bit (or higher) and press `Ctrl + B` to build the project or `Ctrl + R` to run the project.

**Terminal**

Alternatively, you can navigate to the `UNO` folder and run the setup script (file named *setup.sh*):
```bash
   $ cd UNO 
   $ ./setup.sh
```

Script is setup to build the project using `CMake` by default. Before compiling, you will need to edit *CMakeLists.txt* file to match your Qt6 installation directory.

Run with `--help` option to see additional commands.

**IMPORTANT**: If you decide to compile the project using `qmake`, make sure the version of Qt it's using is >= 6.2.2. You can check qmake's version by running `qmake --version`.

After trying to compile, if you recieve the error `cannot find -lGL`, run
```bash
   $ sudo apt-get install libgl1-mesa-dev
```

Currently tested and working only on `Ubuntu`'s distribution of Linux.

## How to play

### Game settings

After starting the application you are met with the Main Menu window. From there you can choose whether you want to see the leaderboards, setup sound volume, play against AI (computer bots) or against real people. If you choose to play against real people you can select whether you want to host the game or join one.

Next you will be asked to choose a gamemode which will determine the modifiers of the game you will be playing (more on this in **Modifiers** subsection) and how many players in total will there be. Finally, you are asked to choose your name (which must be a **non-empty** string) for the game you will be joining.

If you chose to play against real people, you will enter a lobby and wait for the host to start the game. Assuming not all of the available spots are filled with real people, the bots will replace them.

### Start of the game

After you (or the host, in online mode) starts the game, all of the players will be given 7 cards and one card will go face-up on the pile. First player chooses a card in hand, but the card must either match the number, color or type of the card that's on top of the pile. If you have a *wild* card in hand (colored black) you can always use it. If, at some point, a player can't (or won't) play the card he must draw one card from the pile which he can then play if he wants and it matches some of the previously mentioned attributes of the card on top of the pile. If the player can't (or won't) play the card even after that, he finishes his turn and the next player to play is the one right next to him going in a counter clockwise direction.

### Special Cards

As hinted before, there are several types of "special cards" which make the game more interesting and unpredictable. Those cards and their effects are:

- **Skip**: Next player skips his turn
- **Reverse**: Game direction changes from counter clockwise to clockwise (or reverse)
- **Draw Two**: Next player draws 2 cards and skips his turn
- **Wild**: Current player picks a desired color
- **Wild Draw Four**: Current player picks a desired color, next player draws 4 cards and skips his turn

### End of the game

If a player is left with 1 card he is required to say **UNO** to notify other players he can finish the game when his next turn comes. If he forgets to say it before he finishes his turn, he is forced to draw two cards.

When the player places his final card on the pile, the game is over, he is declared the winner while others are ranked by how many points they achieved durning the game (more in **Modifiers** subsection).

### Modifiers

For our application, we have chosen to modify the original game by adding in score in the form of points a player achieves by placing down cards or winning the game. Winner of a game round gets **100** points while the following table tells you how much placing each card is worth:

| Type           | Points     |
|----------------|------------|
| Numeric        | Face value |
| Skip           | 20         |
| Draw Two       | 20         |
| Reverse        | 20         |
| Wild           | 50         |
| Wild Draw Four | 50         |

We also made 3 additional gamemodes (alongside the standard one, which we call *Classic Uno*) with the following descriptions:

- **Elimination**: Instead of a player winning when they reach 0 cards, the player that played before them gets eliminated and the current player inherits their cards. This 
   goes on until there are 2 players left, in which case it acts the same as *Classic*.
- **Zeros and Sevens**: Here, next to special card types, playing a numeric card with the value 0 or 7 also has an effect. If you play a 0, each player's cards are given to
   the next player in order according to the game direction, and if you play a 7, you may choose a player to swap your cards with.
- **Deadly Uno**: Here, players get eliminated when they have no playable cards on their turn.

## Demo video

On the following [link](https://www.youtube.com/watch?v=p2Cm_Rcou-Y) you can find a Youtube video that represents the demo video for this project.

## Licence for additional resources

- All background and icon images are downloaded from [Flaticon](https://www.flaticon.com/) site.
- Main menu background music is taken from [here](https://www.youtube.com/watch?v=iIHDQ0FONik).
- Lobby background music is taken from [here](https://www.youtube.com/watch?v=v9PRpIezoUY).
- Game background music is taken from [here](https://www.youtube.com/watch?v=Byz6960tWxQ&t=131s).

# UML diagrams

## Use case diagram
<img src="./images/use-case-diagram.png" />

- [Link to use case diagram](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/diagrams/image/use-case_diagram.pdf)

## Component diagram
<img src="./images/component-diagram.png" />

- [Link to component diagram](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/diagrams/image/component_diagram.pdf)

## Class diagram
<img src="./images/class-diagram.png" />

- [Link to class diagram](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/diagrams/image/class%20diagram.pdf)


***Use cases***:
- Playing a single game
- Saving the final result
- Displaying best results
- Game settings
- Setting up game mode
- Choosing the number of players


***Actors*** :
- Player


## Game settings

**Description**: Before starting a game, the player can choose a possible way to play the game, including:'Online' and 'Play vs AI'.

**Actors**:
-The Player: chooses appropriate settings

**Preconditions**:
-The application is running and the main menu is opened.

**Postconditions**: The player set up the game and continued to the "Setting up game mode" use case.

**Main scenario**:
1. The Player (you) started the game.
2. The game displays a possible ways to play the game.
3. The Player chooses one of the available ways. 
    1. If the Player chose 'Online', then:
        1. The Game continues online with multiple players.
    2. If the Player chose 'Offline', then:
        1. The Game continues offline with the computer as opponent.
4. The Game then continues to *Setting up game mode*

**Alternative scenarios**:
-A1: **Unexpected application exit**: The settings are discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: Internet connection for 'Online' and 'Tournament' mode.

**Additional information**: None.
    
<img src="./images/sequence/game-settings.png" />

- [Link to sequence diagram - Game settings](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/game-settings.png)


## Setting up game mode

**Description**: Before starting a game, the player can choose from a selection of possible game modes, including, but not limited to: 'Classic', 'Zeros and Sevens', 'Deadly Uno', 'Elimination Uno', etc..

**Actors**:
- The Player: gets to pick between the possible game modes

**Preconditions**: 
- "Game settings" use case

**Postconditions**: The player has chosen their desired game mode and continued to the "Choosing the number of players" use case.

**Main scenario**:
1. The Player (you) finishes with *Game settings*.
2. The Game displays a list of possible game modes to the Player.
3. The Player chooses one of the available game modes.
    1. If the Player chose 'Classic', then:
        1. The Game continues normally.
    2. If the Player chose 'Zeros and Sevens', then:
        1. The Game adds a flag to every 0 and 7 card in the deck, signifying that, when played, they have bonus effects.
    3. If the Player chose 'Deadly Uno', then:
        1. The Game enables eliminations.
        2. The Game sets a flag for elimination when a player has no playable cards.
    4. If the Player chose 'Elimination Uno', then:
        1. The Game enables eliminations.
        2. The Game sets a flag for elimination when a player reaches 0 cards, eliminating the player responsible.
        3. The Game adds a flag for the player who reached 0 cards to inherit the cards from the eliminated player.
4. The Game then continues to *Choosing the number of players*

**Alternative scenarios**:
- A1: **Unexpected application exit**: The selection is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./images/sequence/setting-up-game-mode.png" />

- [Link to sequence diagram - Setting up game mode](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/setting-up-game-mode.png)


## Choosing the number of players

**Description**: Before starting a game, the player can choose the number of players participating in a game.

**Actors**:

- The Player: gets to pick the number of opponents, depending on the game mode.

**Preconditions**: 

- "Game settings" use case.

**Postconditions**: 

- The player has chosen the number of players in a game and started the game.

**Main scenario**:

1. The Player (you) finishes with *Setting up game mode*.
2. The game displays a list of possible numbers of players.
3. The Player chooses one of the available numbers of players.
4. The Game then continues to *Playing a single game*.

**Alternative scenarios**:

- A1: **Unexpected application exit**: The selection is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: Internet connection for 'Online' and 'Tournament' mode.

**Additional information**: None.

<img src="./images/sequence/choosing-the-number-of-players-2.png" />

- [Link to sequence diagram - Choosing the number of players](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/choosing-the-number-of-players-2.png)


## Playing a single game

**Description**: Seven cards are dealt to each player. Players take turns matching a card in their hand with the current card shown on top of the pile either by color or number. Special action cards deliver game-changing moments and provide help in defeating the opponents. The first player to rid themselves of all the cards in their hand before their opponents is the winner.

**Actors**:
- The Player: initiates the game and plays against other human or AI Players
- Human opponents: other players that joined the opened online lobby
- AI opponents: bot opponents handled by computer

**Preconditions**: 
- "Choosing the number of players" use case.
- "Game settings" use case
- "Setting up game mode" use case

**Postconditions**: The game is completed, winner is determined and results of the game are displayed to the player.

**Main scenario**:
1. The Player (you) starts the game by pressing one of the buttons displaying available *game modes*.
2. The game initializes an instance of a game with AI or human opponents.
3. The game shuffles the deck
4. The game gives seven cards from the top of the deck to opponents and to the player
5. Draw a random and *non-special* card from the deck put it at the top of the pile.
6. While the game is not over, the following steps are repeated:
    1. The game determines whose turn to play is it.
    2. If it's your turn to play, then:
        1. Did the previous player place a *skip card* at the top of the pile? If he did, jump to 10.
        2. Did the previous player place a *wild draw four* or *draw two* card at the top of the pile? If he didn't, jump to 4.
        3. Draw the required number of cards from the deck (two or four) and jump to 10.
        4. Do you have a card in your hand that matches either the color or the number with the card shown on top of the pile? If you don't, jump to 12.
        5. Do you want to place one of the matching cards on top of the pile? If you don't, jump to 12.
        6. Is your card a Wild one? If it isn't, jump to 8.
        7. Choose a color
        8. Place the card.
        9. If you have one card remaining jump to 11.
        10. Finish your turn.
        11. If you forgot to say "UNO!", draw two cards from the deck as punishment and jump to 10.
        12. If you haven't already drawn the card from the top of the deck, do it and jump to 4. Else jump to 10.
    3. If it's the opponents turn to play, then wait for your turn while other players play their own turns.
    4. If there are no more cards in the deck, flip the pile, reshuffle it and make it the new deck.
    5. If a player rid himself of the last card in his hand, the game is over with him as the winner.
7. When the game is finished, the application displays the results based on remaining cards in each player's hand to all the participants (the player with zero cards remaining is considered the winner).

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./images/sequence/playing-a-single-game.png" />

- [Link to sequence diagram - Playing a single game](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/playing-a-single-game.png)


## Saving the final result

**Description**: After the game is over, the player enters his name and the application stores the results.

**Actors**:
-The Player: he finished the game and enters his name

**Preconditions**:
-The application is running and the player finished playing one game

**Postconditions**: Informations about the game played are saved

**Main scenario**:
1. The application creates a name dialog
2. Name dialog is displayed to the player
3. The player enters his name
4. The application stores player's name and number of points won


**Alternative scenarios**:
-A1: **Unexpected application exit**: The data are not saved and the application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

<img src="./images/sequence/saving-the-final-result-change.png" />

- [Link to sequence diagram - Saving the final result](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/saving-the-final-result-change.png)


## Displaying best results

**Description**: The application displays the final results as leaderboard. After displaying the leaderboard, the player has the option to return to the main menu.

**Actors**: 
- The Player: chooses an option from the main menu

**Preconditions**:
- The application is running and the main menu is opened.

**Postconditions**: 
- The ten best players are shown.

**Main scenario**:
1. The Player chooses a button "Leaderboard" from main menu.
2. The application displays a list of the fifteen best results.
3. The Player chooses a button "Back" and then returns to the main menu.

**Alternative scenarios**:
- A1: **Unexpected application exit**: The selection is discarded and the application closes**

**Subscenarios**: None

**Special requirements**: None

**Additional information**: None

<img src="./images/sequence/saving-the-final-result-change.png" />

- [Link to sequence diagram - Displaying best result](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno/-/blob/master/specification/sequence-diagrams/image/displaying-best-results__1_.png)