# Saving the final result

**Description**: After the game is over, the player enters his name and the application stores the results.

**Actors**:
-The Player: he finished the game and enters his name

**Preconditions**:
-The application is running and the player finished playing one game

**Postconditions**: Informations about the game played are saved

**Main scenario**:
1. The application creates a name dialog
2. Name dialog is displayed to the player
3. The player enters his name
4. The application stores player's name and number of points won


**Alternative scenarios**:
-A1: **Unexpected application exit**: The data are not saved and the application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
