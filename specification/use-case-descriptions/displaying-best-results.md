# Displaying best results

**Description**: The application displays the final results as leaderboard. After displaying the leaderboard, the player has the option to return to the main menu.

**Actors**: 
- The Player: chooses an option from the main menu

**Preconditions**:
- The application is running and the main menu is opened.

**Postconditions**: 
- The ten best players are shown.

**Main scenario**:
1. The Player chooses a button "Leaderboard" from main menu.
2. The application displays a list of the ten best results.
3. The Player chooses a button "Back" and then returns to the main menu.

**Alternative scenarios**:
- A1: **Unexpected application exit**: The selection is discarded and the application closes**

**Subscenarios**: None

**Special requirements**: None

**Additional information**: None

