# Game settings

**Description**: Before starting a game, the player can choose a possible way to play the game, including:'Online', 'Offline' and 'Tournament'.

**Actors**:
-The Player: chooses appropriate settings

**Preconditions**:
-The application is running and the main menu is opened.

**Postconditions**: The player set up the game and continued to the "Setting up game mode" use case.

**Main scenario**:
1. The Player (you) started the game.
2. The game displays a possible ways to play the game.
3. The Player chooses one of the available ways. 
    1. If the Player chose 'Online', then:
        1. The Game continues online with multiple players.
    2. If the Player chose 'Tournament', then:
        1. The Game continues online with multiple players.
        2. The game enables a tournament competition.
    3. If the Player chose 'Offline', then:
        1. The Game continues offline with the computer as opponent.
4. The Game then continues to *Setting up game mode*

**Alternative scenarios**:
-A1: **Unexpected application exit**: The settings are discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: Internet connection for 'Online' and 'Tournament' mode.

**Additional information**: None.
    
