## Choosing the number of players

**Description**: Before starting a game, the player can choose the number of players participating in a game.

**Actors**:

- The Player: gets to pick the number of opponents, depending on the game mode.

**Preconditions**: 

- "Game settings" use case.

**Postconditions**: 

- The player has chosen the number of players in a game and started the game.

**Main scenario**:

1. The Player (you) finishes with *Setting up game mode*.
2. The game displays a list of possible numbers of players.
3. The Player chooses one of the available numbers of players.
4. The Game then continues to *Playing a single game*.

**Alternative scenarios**:

- A1: **Unexpected application exit**: The selection is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: Internet connection for 'Online' and 'Tournament' mode.

**Additional information**: None.
