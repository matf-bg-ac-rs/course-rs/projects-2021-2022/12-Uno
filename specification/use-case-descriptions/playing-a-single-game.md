# Playing a single game

**Description**: Seven cards are dealt to each player. Players take turns matching a card in their hand with the current card shown on top of the pile either by color or number. Special action cards deliver game-changing moments and provide help in defeating the opponents. The first player to rid themselves of all the cards in their hand before their opponents is the winner.

**Actors**:
- The Player: initiates the game and plays against other human or AI Players
- Human opponents: other players that joined the opened online lobby
- AI opponents: bot opponents handled by computer

**Preconditions**: 
- "Choosing the number of players" use case.
- "Game settings" use case
- "Setting up game mode" use case

**Postconditions**: The game is completed and the winner is determined.

**Main scenario**:
1. The Player (you) starts the game by pressing one of the buttons displaying available *game modes*.
2. The game initializes an instance of a game with AI or human opponents.
3. The game shuffles the deck
4. The game gives seven cards from the top of the deck to opponents and to the player
5. Draw a random and *non-special* card from the deck put it at the top of the pile.
6. While the game is not over, the following steps are repeated:
    1. The game determines whose turn to play is it.
    2. If it's your turn to play, then:
        1. Did the previous player place a *skip card* at the top of the pile? If he did, jump to 10.
        2. Did the previous player place a *wild draw four* or *draw two* card at the top of the pile? If he didn't, jump to 4.
        3. Draw the required number of cards from the deck (two or four) and jump to 10.
        4. Do you have a card in your hand that matches either the color or the number with the card shown on top of the pile? If you don't, jump to 12.
        5. Do you want to place one of the matching cards on top of the pile? If you don't, jump to 12.
        6. Is your card a Wild one? If it isn't, jump to 8.
        7. Choose a color
        8. Place the card.
        9. If you have one card remaining jump to 11.
        10. Finish your turn.
        11. If you forgot to say "UNO!", draw two cards from the deck as punishment and jump to 10.
        12. If you haven't already drawn the card from the top of the deck, do it and jump to 4. Else jump to 10.
    3. If it's the opponents turn to play, then wait for your turn while other players play their own turns.
    4. If there are no more cards in the deck, flip the pile, reshuffle it and make it the new deck.
    5. If a player rid himself of the last card in his hand, the game is over with him as the winner.
7. When the game is finished, the application displays the results based on remaining cards in each player's hand to all the participants (the player with zero cards remaining is considered the winner).

**Alternative scenarios**:
- A1: **Unexpected application exit**: The game is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.