# Setting up game mode

**Description**: Before starting a game, the player can choose from a selection of possible game modes, including, but not limited to: 'Classic', 'Zeros and Sevens', 'Deadly Uno', 'Elimination Uno', etc..

**Actors**:
- The Player: gets to pick between the possible game modes

**Preconditions**: 
- "Game settings" use case

**Postconditions**: The player has chosen their desired game mode and continued to the "Choosing the number of players" use case.

**Main scenario**:
1. The Player (you) finishes with *Game settings*.
2. The Game displays a list of possible game modes to the Player.
3. The Player chooses one of the available game modes.
	1. If the Player chose 'Classic', then:
		1. The Game continues normally.
	2. If the Player chose 'Zeros and Sevens', then:
		1. The Game adds a flag to every 0 and 7 card in the deck, signifying that, when played, they have bonus effects.
	3. If the Player chose 'Deadly Uno', then:
		1. The Game enables eliminations.
		2. The Game sets a flag for elimination when a player has no playable cards.
	4. If the Player chose 'Elimination Uno', then:
		1. The Game enables eliminations.
		2. The Game sets a flag for elimination when a player reaches 0 cards, eliminating the player responsible.
		3. The Game adds a flag for the player who reached 0 cards to inherit the cards from the eliminated player.
4. The Game then continues to *Choosing the number of players*

**Alternative scenarios**:
- A1: **Unexpected application exit**: The selection is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.

