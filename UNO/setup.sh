#!/bin/sh

RED="\e[31m"
ENDCOLOR="\e[0m"

if [ "$1" = "--help" ]
then
    echo "\nUSAGE: ./setup.sh [options]\n\nOPTIONS:\n\t${RED}-q${ENDCOLOR}\t\t - Build project with qmake \n\t${RED}-r${ENDCOLOR}\t\t - Build and run the application"
    exit
fi

cd ..
mkdir build
cd build

if [ "$1" = "-q" ]
then
    qmake ../UNO
else
    cmake ../UNO
fi

make

if [ "$1" = "-r" ]
then
    ./../build/UNO
fi