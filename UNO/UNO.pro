QT       += core gui \
            multimedia \
            network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

CONFIG += resources_big

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        sources/client.cpp \
	sources/gameGUI.cpp \
	sources/lobby.cpp \
	sources/leaderboards.cpp \
    sources/card.cpp \
    sources/deck.cpp \
    sources/gameclient.cpp \
    sources/gameserver.cpp \
    sources/human.cpp \
    sources/pile.cpp \
    sources/player.cpp \
    sources/main.cpp \
        sources/server.cpp \
    sources/wildcard.cpp \
    sources/game.cpp \
	sources/pc.cpp \
	sources/mainWindow.cpp \
    sources/instructionsWidget.cpp

HEADERS += \
        headers/client.h \
	headers/gameGUI.h \
	headers/gameParameters.h \
    headers/card.h \
    headers/deck.h \
    headers/gameclient.h \
    headers/gameserver.h \
    headers/human.h \
    headers/pile.h \
    headers/player.h \
        headers/server.h \
    headers/wildcard.h \
    headers/game.h \
	headers/pc.h \
	headers/mainWindow.h \
	headers/leaderboards.h \
	headers/lobby.h \
    headers/instructionsWidget.h

FORMS += \
	forms/gameGUI.ui \
	forms/mainWindow.ui \
	forms/lobby.ui \
    forms/instructionsWidget.ui

RESOURCES += \
    resources/application.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
