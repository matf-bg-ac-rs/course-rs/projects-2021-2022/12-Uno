# Project Uno

**UNO** is a multiplayer card game application that can be played against bots or against real people with the player amount being either 2, 4 or 8. The goal of the game is for the player to rid himself of all the cards in his hand and while doing so, earn points by placing cards on the pile which is how the final score of that player for that game is determined. More information on how to play the game in **How to play** section.

## How to setup

### Prerequisites

- gcc / g++
- Qt 6.2.2
- Qt6Multimedia

First of all, download package information from all configured sources by running
```bash
   $ sudo apt-get update
```

Next, install gcc / g++ by typing:
```bash
   $ sudo apt-get -y install gcc g++
```

Unfortunately, Qt6 and Qt Multimedia Add-On module can **only** (on Ubuntu) be added from the MaintananceTool (located in Qt's install directory) because you can't install it using Ubuntu's packaging tool (apt).

### Setup

**GUI**

Open the project in QtCreator, setup your kits so it's using Desktop Qt 6.2.2 GCC 64-bit (or higher) and press `Ctrl + B` to build the project or `Ctrl + R` to run the project.

**Terminal**

Alternatively, you can navigate to the `UNO` folder and run the setup script (file named *setup.sh*):
```bash
   $ cd UNO 
   $ ./setup.sh
```

Script is setup to build the project using `CMake` by default. Before compiling, you will need to edit *CMakeLists.txt* file to match your Qt6 installation directory.

Run with `--help` option to see additional commands.

**IMPORTANT**: If you decide to compile the project using `qmake`, make sure the version of Qt it's using is >= 6.2.2. You can check qmake's version by running `qmake --version`.

After trying to compile, if you recieve the error `cannot find -lGL`, run
```bash
   $ sudo apt-get install libgl1-mesa-dev
```

Currently tested and working only on `Ubuntu`'s distribution of Linux.

## How to play

### Game settings

After starting the application you are met with the Main Menu window. From there you can choose whether you want to see the leaderboards, setup sound volume, play against AI (computer bots) or against real people. If you choose to play against real people you can select whether you want to host the game or join one.

Next you will be asked to choose a gamemode which will determine the modifiers of the game you will be playing (more on this in **Modifiers** subsection) and how many players in total will there be. Finally, you are asked to choose your name (which must be a **non-empty** string) for the game you will be joining.

If you chose to play against real people, you will enter a lobby and wait for the host to start the game. Assuming not all of the available spots are filled with real people, the bots will replace them.

### Start of the game

After you (or the host, in online mode) starts the game, all of the players will be given 7 cards and one card will go face-up on the pile. First player chooses a card in hand, but the card must either match the number, color or type of the card that's on top of the pile. If you have a *wild* card in hand (colored black) you can always use it. If, at some point, a player can't (or won't) play the card he must draw one card from the pile which he can then play if he wants and it matches some of the previously mentioned attributes of the card on top of the pile. If the player can't (or won't) play the card even after that, he finishes his turn and the next player to play is the one right next to him going in a counter clockwise direction.

### Special Cards

As hinted before, there are several types of "special cards" which make the game more interesting and unpredictable. Those cards and their effects are:

- **Skip**: Next player skips his turn
- **Reverse**: Game direction changes from counter clockwise to clockwise (or reverse)
- **Draw Two**: Next player draws 2 cards and skips his turn
- **Wild**: Current player picks a desired color
- **Wild Draw Four**: Current player picks a desired color, next player draws 4 cards and skips his turn

### End of the game

If a player is left with 1 card he is required to say **UNO** to notify other players he can finish the game when his next turn comes. If he forgets to say it before he finishes his turn, he is forced to draw two cards.

When the player places his final card on the pile, the game is over, he is declared the winner while others are ranked by how many points they achieved durning the game (more in **Modifiers** subsection).

### Modifiers

For our application, we have chosen to modify the original game by adding in score in the form of points a player achieves by placing down cards or winning the game. Winner of a game round gets **100** points while the following table tells you how much placing each card is worth:

| Type           | Points     |
|----------------|------------|
| Numeric        | Face value |
| Skip           | 20         |
| Draw Two       | 20         |
| Reverse        | 20         |
| Wild           | 50         |
| Wild Draw Four | 50         |

We also made 3 additional gamemodes (alongside the standard one, which we call *Classic Uno*) with the following descriptions:

- **Elimination**: Instead of a player winning when they reach 0 cards, the player that played before them gets eliminated and the current player inherits their cards. This 
   goes on until there are 2 players left, in which case it acts the same as *Classic*.
- **Zeros and Sevens**: Here, next to special card types, playing a numeric card with the value 0 or 7 also has an effect. If you play a 0, each player's cards are given to
   the next player in order according to the game direction, and if you play a 7, you may choose a player to swap your cards with.
- **Deadly Uno**: Here, players get eliminated when they have no playable cards on their turn.

## Demo video

On the following [link](https://www.youtube.com/watch?v=p2Cm_Rcou-Y) you can find a Youtube video that represents the demo video for this project.

## Licence for additional resources

- All background and icon images are downloaded from [Flaticon](https://www.flaticon.com/) site.
- Main menu background music is taken from [here](https://www.youtube.com/watch?v=iIHDQ0FONik).
- Lobby background music is taken from [here](https://www.youtube.com/watch?v=v9PRpIezoUY).
- Game background music is taken from [here](https://www.youtube.com/watch?v=Byz6960tWxQ&t=131s).


## Developers

- [Darko Mladenovski, 278/2017](https://gitlab.com/dare21)
- [Marija Božić, 241/2018](https://gitlab.com/Marija2402)
- [Jelena Čanković, 96/2018](https://gitlab.com/jelena1406)
- [Pavle Cvejović, 24/2018](https://gitlab.com/pavle991)
- [Viktor Novaković, 92/2018](https://gitlab.com/vita-ride)






